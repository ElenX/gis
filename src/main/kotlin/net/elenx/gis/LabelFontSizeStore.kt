package net.elenx.gis

import io.reactivex.Emitter
import io.reactivex.Observable

class LabelFontSizeStore
{
    var lastValue = 9.0

    val observable: Observable<Double> = Observable
        .create<Double> { emitter = it }
        .cacheWithInitialCapacity(1)
        .startWith(lastValue)

    private var emitter: Emitter<Double>? = null

    init
    {
        observable.subscribe { lastValue = it }
    }

    fun updateTo(newLabelFontSize: Double) = emitter!!.onNext(newLabelFontSize)
}
