package net.elenx.gis

import javafx.event.EventHandler
import javafx.event.EventTarget
import javafx.scene.control.Label
import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import org.kodein.di.generic.instance
import tornadofx.*

private const val borderThickness = 3.0
private const val innerRadius = 30.0

const val outerRadius = innerRadius + borderThickness

class Vertex(parent: EventTarget, centerX: Double, centerY: Double, val etiquette: String)
{
    val outerCircle: Circle = Circle(centerX, centerY, outerRadius)
    val listeners = mutableSetOf<() -> Unit>()

    private val innerCircle: Circle = Circle(centerX, centerY, innerRadius)
    private val label = Label(etiquette)
    private val labelFontSizeStore: LabelFontSizeStore by kodein.instance()
    private val graph: Graph by kodein.instance()

    init
    {
        with(outerCircle)
        {
            attachTo(parent)
            fill = Color.BLACK
            onMousePressed = mousePressedHandler()
        }

        with(innerCircle)
        {
            attachTo(parent)
            style { fill = Color.LIGHTGRAY }
            onMouseDragged = mouseDraggedHandler()
            onMousePressed = mousePressedHandler()
        }

        with(label)
        {
            attachTo(parent)
            labelFor = innerCircle
            layoutX = labelFor(innerCircle.centerX)
            layoutY = labelFor(innerCircle.centerY)
        }

        labelFontSizeStore.observable.subscribe { changeFontSizeTo(it) }
    }

    private fun activate()
    {
        outerCircle.fill = Color.RED
    }

    private fun deactivate()
    {
        outerCircle.fill = Color.BLACK
    }

    private fun <T: MouseEvent> mouseDraggedHandler() = EventHandler<T>
    {
        if(it.isPrimaryButtonDown)
        {
            setOf(innerCircle, outerCircle).forEach { circle: Circle -> circle.centerTo(it.x, it.y) }
            with(label)
            {
                layoutX = labelFor(it.x)
                layoutY = labelFor(it.y)
            }

            listeners.forEach { it() }
        }
    }

    private fun <T: MouseEvent> mousePressedHandler() = EventHandler<T>
    {
        if(it.isSecondaryButtonDown) when
        {
            FutureEdge.from == this ->
            {
                FutureEdge.from = null
                deactivate()
            }
            FutureEdge.from == null ->
            {
                FutureEdge.from = this
                activate()
            }
            else ->
            {
                graph.edge(FutureEdge.from!!, this)
                FutureEdge.from?.deactivate()
                deactivate()
                FutureEdge.from = null
            }
        }
    }

    private fun labelFor(center: Double) = center - innerRadius / 2
    private fun changeFontSizeTo(newFontSize: Double) = label.style { fontSize = Dimension(newFontSize, Dimension.LinearUnits.mm) }
}

private fun Circle.centerTo(x: Double, y: Double)
{
    centerX = x
    centerY = y
}
