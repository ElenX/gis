package net.elenx.gis

import javafx.scene.layout.BorderPane
import org.kodein.di.generic.instance
import tornadofx.*

class MainView : View("Grafy i Sieci 18L")
{
    override val root: BorderPane by kodein.instance()

    private val graph: Graph by kodein.instance()
    private val menuController: MenuController by kodein.instance()

    init
    {
        with(root)
        {
            top = menubar {
                menu("File") {
                    item("Print", "Ctrl+P")
                    item("Export as image", "Ctrl+E").also { it.setOnAction { menuController.exportAsImage() } }
                    item("Show incidence matrix", "Ctrl+M").also { it.setOnAction { menuController.showIncidenceMatrix() } }
                    item("Quit", "Ctrl+Q")
                }
                menu("Edit") {
                    item("Copy to clipboard", "Ctrl+C")
                    item("Change font size", "Ctrl+F").also { it.setOnAction { menuController.changeFontSize() } }
                }
            }

            center = graph.anchorPane
        }
    }
}
