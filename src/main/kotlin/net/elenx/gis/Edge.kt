package net.elenx.gis

import javafx.event.EventTarget
import javafx.scene.shape.Line
import tornadofx.*
import kotlin.math.atan
import kotlin.math.cos
import kotlin.math.sin

private const val edgeThickness = 1.0

class Edge(parent: EventTarget, val from: Vertex, val to: Vertex, val id: Int, val weight: Int = 1)
{
    private val line = Line()

    init
    {
        with(line)
        {
            attachTo(parent)
            style { strokeWidth = Dimension(edgeThickness, Dimension.LinearUnits.mm) }
        }

        redraw()
    }

    fun redraw()
    {
        val startX = from.outerCircle.centerX
        val startY = from.outerCircle.centerY

        val endX = to.outerCircle.centerX
        val endY = to.outerCircle.centerY

        val tangent = (endY - startY) / (endX - startX)
        val angle = atan(tangent)

        val cosine = outerRadius * cos(angle)
        val sine = outerRadius * sin(angle)

        if(startX >= endX)
        {
            line.startX = startX - cosine
            line.startY = startY - sine
            line.endX = endX + cosine
            line.endY = endY + sine
        }
        else
        {
            line.startX = startX + cosine
            line.startY = startY + sine
            line.endX = endX - cosine
            line.endY = endY - sine
        }
    }
}
