package net.elenx.gis

import javafx.scene.layout.AnchorPane
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Pane
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val kodein = Kodein {
    bind<Graph>() with singleton { Graph() }
    bind<BorderPane>() with singleton { BorderPane() }
    bind<Pane>() with singleton { Pane() }
    bind<MenuController>() with singleton { MenuController(instance(), instance()) }
    bind<AnchorPane>() with singleton { AnchorPane(instance<Pane>()) }
    bind<LabelFontSizeStore>() with singleton { LabelFontSizeStore() }
}
