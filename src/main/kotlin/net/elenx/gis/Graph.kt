package net.elenx.gis

import javafx.event.EventHandler
import javafx.print.Paper
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import org.kodein.di.generic.instance
import tornadofx.*
import kotlin.math.sqrt

class Graph(val vertexes: MutableSet<Vertex> = mutableSetOf(), val edges: MutableSet<Edge> = mutableSetOf())
{
    val pane: Pane by kodein.instance()
    val anchorPane: AnchorPane by kodein.instance()

    private var vertexIndexer = vertexes.size
    private var edgeIndexer = edges.size

    init
    {
        with(anchorPane)
        {
            style { backgroundColor = multi(Color.DARKGRAY) }
        }

        with(pane)
        {
            style { backgroundColor = multi(Color.WHITE) }
            onMousePressed = EventHandler { if(it.isMiddleButtonDown) vertex(it.x, it.y) }
            minHeightProperty().set(Paper.A4.height)

            minWidthProperty().bind(minHeightProperty().div(sqrt(2.0)))
            maxWidthProperty().bind(maxHeightProperty().div(sqrt(2.0)))
        }

        edge(vertex(500.0, 500.0), vertex(200.0, 100.0))
    }

    fun vertex(x: Double, y: Double) = Vertex(pane, x, y, (vertexIndexer++).toString()).also { vertexes += it }
    fun edge(from: Vertex, to: Vertex)
    {
        if(edges.any { (it.from == from && it.to == to) || (it.from == to && it.to == from) }) return

        val edge = Edge(pane, from, to, edgeIndexer++)
        edges += edge

        from.listeners += { edge.redraw() }
        to.listeners += { edge.redraw() }
    }
}
