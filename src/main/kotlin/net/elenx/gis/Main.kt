package net.elenx.gis

import javafx.application.Application
import tornadofx.*

fun main(args: Array<String>) = Application.launch(MainApplication::class.java, *args)

class MainApplication : App()
{
    override val primaryView = MainView::class
}

// 210 mm × 297 mm
