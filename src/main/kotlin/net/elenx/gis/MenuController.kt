package net.elenx.gis

import javafx.embed.swing.SwingFXUtils
import javafx.scene.SnapshotParameters
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import javafx.scene.control.ChoiceDialog
import javafx.scene.control.TextArea
import javafx.scene.image.WritableImage
import javafx.stage.FileChooser
import javax.imageio.ImageIO



class MenuController(private val graph: Graph, private val labelFontSizeStore: LabelFontSizeStore)
{
    fun exportAsImage()
    {
        val file = FileChooser().showOpenDialog(null) ?: return
        val pane = graph.pane
        val writableImage = pane.snapshot(SnapshotParameters(), WritableImage(ceilOf(pane.minWidth), ceilOf(pane.minWidth)))

        ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file)
    }

    private fun ceilOf(d: Double) = Math.ceil(d).toInt()

    fun changeFontSize()
    {
        val dialog = ChoiceDialog<Double>(labelFontSizeStore.lastValue, (6..40).filter { it % 2 == 0 }.map { it.toDouble() })
        dialog.dialogPane.buttonTypes.add(ButtonType.CLOSE)
        dialog.showAndWait().ifPresent(labelFontSizeStore::updateTo)
    }

    fun showIncidenceMatrix()
    {
        val incidenceMatrix = graph
            .edges
            .map { createColumn(graph.vertexes.size, it.from.etiquette, it.to.etiquette) }
            .map { it.map { it.toString() }.reduceRight { s1, s2 -> "$s1 | $s2" } }
            .reduceRight { s1, s2 -> "$s1\n$s2" }

        val alert = Alert(Alert.AlertType.INFORMATION)
        alert.title = "Incidence matrix"
        alert.headerText = null
        alert.dialogPane.buttonTypes.removeIf { true }
        alert.dialogPane.buttonTypes.add(ButtonType.OK)

        val textArea = TextArea(incidenceMatrix)
        textArea.isWrapText = false

        alert.dialogPane.content = textArea
        alert.showAndWait()
    }

    private fun createColumn(size: Int, from: String, to: String) = Array(size, { if(it.toString() == from || it.toString() == to) 1 else 0 } )
}
